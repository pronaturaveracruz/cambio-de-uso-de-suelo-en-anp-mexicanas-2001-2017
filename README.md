![Minimal R
Version](https://img.shields.io/badge/R%3E%3D-3.2.0-blue.svg)
![License](https://img.shields.io/badge/License-LGPL-blue.svg)


# Análisis de cambio de cobertura por Área Natural Protegida en México
   Las ANP son un conjunto de demarcaciones que buscan preservar la biodiviersidad o los servicios ambientales del país; su categoría de protección no las exenta de las presiones económicas y sociales de sus inmediaciones. En este ejercicio presentamos una visualización de su efectividad como protección, a través de la cobertura de suelo, que sintetiza muy bien la integridad de un ecosistema y que es muy fácil de conseguir y procesar, en comparación con registros de biodiversidad hechos en campo. _Este trabajo se enfoca en la pérdida de cobertura primaria_

### Series de uso de suelo y vegetación de INEGI
   En un periodo de aproximadamente 4 años, el INEGI genera una capa actualizada de uso de suelo y vegetación (USV) conocida como Serie, a escala 1:250,000, la cual puede ser una resolución baja, pero es suficiente para detectar cambios a nivel de paisaje en las ANP del país. Estas incluyen cerca de 200 categorías de uso de suelo, las cuales varían levemente entre Serie y derivan de los ecosistemas originales (bosques de pino, selvas, matorrales, bosques de encino, ...), así como sus respectivas vegetaciones secundarias (arbórea, arbustiva y herbácea) y las coberturas antropizadas (agrícola, acuícola, urbana, pastizales para pastoreo). En este ejercicio fueron agrupadas (revisar el código para más detalle) en vegetación primaria, vegetación secundaria y antropizado, para tener una comparabilidad entre distintos ecosistemas y poder visualizar mejor el cambio. _Se excluye la Serie 1 por problemas de compatibilidad en las categorías con las demás series_

### Sobre el proceso de los datos y las visualizaciones
    El script "anp_stats_logic" hace una intersección espacial entre cada Serie de USV y las ANP para obtener un dataframe del área de cada cobertura principal por ANP por Serie.
    Por la cantidad de ANP y por su diferencia en tamaño (de cientos a millones de hectáreas) se dividieron en 16 grupos por tamaño, para hacer gráficas por cada grupo, en las carpetas "graficas" del repositorio se encuentran las visualizaciones de los grupos 6 al 16, las demás son generables con los scripts.

# El cambio de uso de suelo en ANP mexicanas
   En los archivos png del repositorio se encuentran gráficas para cada tipo de cobertura (primaria, secundaria y antrópica), que coinciden con los grupos de tamaños de ANP; las gráficas incluyen los mismos colores de línea para cada ANP, para facilitar su comparación, sobre todo en casos de traslape con otras en las gráficas de cobertura secundaria y antrópica.

   Como ejemplo tenemos las siguientes gráficas para un mismo grupo de tamaño, 
<img src="/graficas/1_vegetacion_primaria/veg_prim14.png"  width="500" height="600">

<img src="/graficas/2_vegetacion_secundaria/veg_sec14.png"  width="500" height="600">

<img src="/graficas/3_antropica/antropica14.png"  width="500" height="600">

En estas se puede ver, por ejemplo, que en Los Tuxtlas ha habido una transición de vegetación primaria a coberturas antrópicas, mientras que el cambio en Manantlán, La Concordia y Papigochic ha sido hacia coberturas secundarias, estos casos de transición a vegetación secundaria pueden ser indicativos de actividades de tala ilegal.


